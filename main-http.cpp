//need the lib https://os.mbed.com/teams/sandbox/code/http-example/
//should have folder easy-connect, mbed-http, source
//all added cpp and h file need to be in the source folder

#include "select-demo.h"

#if DEMO == DEMO_HTTP

#include "mbed.h"
#include "easy-connect.h"
#include "http_request.h"
#include "ov2640.h"
#include <algorithm>


//#define REQ_BUFFER_LEN 1024 * 16

//Serial CAM(p13, p14, 921600); // tx, rx

camHelper myCam;

char NUMBER_CHUNK = 0;
size_t totalImageData = 0;

using namespace std;

Serial pc(USBTX, USBRX);

Timer t;

char file_header1[] =
    "--asdfcam1t\r\n"
    "Content-Disposition: form-data; name=\"media\"; filename=\"0_";

char file_header2[] = ".jpg\"\r\n"
                      "Content-Type: image/jpeg\r\n\r\n";
const size_t file_header_lengthTotal = 200;//strlen(file_header2)+strlen(file_header1) + 10;
char file_header[file_header_lengthTotal];

uint32_t currentpicture = 0;

void editHeader(uint32_t pos)
{
    memset(file_header, 0, file_header_lengthTotal);
    memcpy(file_header, file_header1, strlen(file_header1));
    char tmpbuffer[8];
    sprintf(tmpbuffer, "%d", pos);
    strcat(file_header, tmpbuffer);
    strcat(file_header, file_header2);
}

void setCamTime_header()
{
    //to do
}

const char file_footer[] =
    "\r\n--asdfcam1t--\r\n"
    ;

//char req_buffer[REQ_BUFFER_LEN] __attribute__((section("AHBSRAM1")));

enum ChunkState {
    SEND_HEADER,
    SEND_BODY,
    SEND_FOOTER,
    END_REQUEST
};
ChunkState chunk_state = SEND_HEADER;

/*char buffer[IMAGE_CHUNK_SIZE];*/
int chunk_index = 0;

void clearBuffer()
{
    memset(tab, 0, IMAGE_CHUNK_SIZE);
}

const void * get_chunk(size_t* out_size)
{
    
    switch(chunk_state) {
        case SEND_HEADER: {
            editHeader(++currentpicture);
            printf("new header : \n\r %s\n\r", file_header);
            chunk_state = SEND_BODY;
            *out_size = strlen(file_header);

            chunk_index = 0;

            return file_header;
        }
        case SEND_BODY: {   
            //t.start();    
            myCam.read_fifo_test(IMAGE_CHUNK_SIZE);
            //myCam.read_fifo_new(IMAGE_CHUNK_SIZE);
            *out_size = myCam.g_frame_size_in_bytes;
            
            if(myCam.ENDOFJPG) {
                chunk_state = SEND_FOOTER;
            }
            //t.stop();
            //printf("buffer generated in %d ms\n\r", t.read_ms());
            //t.reset(); 
            return tab;
        }
        case SEND_FOOTER: {
            chunk_state = END_REQUEST;
            *out_size = (size_t) sizeof(file_footer);
            return file_footer;
        }
        default: {
            *out_size = 0;
            chunk_state = SEND_HEADER;
            return NULL;
        }
    }
}

void dump_response(HttpResponse* res)
{
    pc.printf("Status: %d - %s\r\n", res->get_status_code(), res->get_status_message().c_str());
    if(res->get_status_code() == 0) {
        NVIC_SystemReset();
    }
}


int main()
{    
    pc.baud(921600);
    pc.printf("==== START ====\n\r");
    myCam.init();

    // Connect to the network (see mbed_app.json for the connectivity method used)
    NetworkInterface* network = easy_connect(true);
    if (!network) {
        pc.printf("Cannot connect to the network, see serial output\r\n");
        return 1;
    }

    pc.printf("\n----- Setting up TCP connection -----\n\r");

    TCPSocket* socket = new TCPSocket();
    nsapi_error_t open_result = socket->open(network);
    if (open_result != 0) {
        pc.printf("Opening TCPSocket failed... %d\n\r", open_result);
        return 1;
    }

    //nsapi_error_t connect_result = socket->connect("192.168.0.102", 8080);
    //nsapi_error_t connect_result = socket->connect("108.171.92.103", 3000);
    nsapi_error_t connect_result = socket->connect("192.168.0.103", 8080);
    if (connect_result != 0) {
        pc.printf("Connecting over TCPSocket failed... %d\n\r", connect_result);
        return 1;
    }

    pc.printf("\n----- TCP done -----\n\r");

    while(1) {
        //wait_ms(10);
        myCam.takePicture();

        // POST
        {
            HttpRequest* post_req = new HttpRequest(socket, HTTP_POST, "http://192.168.0.103:8080/upload/"); // <-- commence avec celle là, je vais voir ce que tu envois dans la console
            //HttpRequest* post_req = new HttpRequest(socket, HTTP_POST, "http://108.171.92.103:3000/upload/"); // <-- commence avec celle là, je vais voir ce que tu envois dans la console

            post_req->set_header("Content-Type", "multipart/form-data; boundary=asdfcam1t");

            pc.printf("\n----- HTTP POST -----\n\r");

            HttpResponse* post_res = post_req->send(&get_chunk);
            if (!post_res) {
                pc.printf("HttpRequest failed (error code %d)\n\r", post_req->get_error());
                NVIC_SystemReset();
                //return 1;
            }else{

                pc.printf("\n----- HTTP POST response -----\n\r");
                dump_response(post_res);
            }

            delete post_req;
        }
    }

    pc.printf("==== END ====\n\r");

    delete socket;

    Thread::wait(osWaitForever);

}

#endif
