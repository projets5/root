#include "mbed.h"
#include "ov2640_regs.h"

#define IMAGE_CHUNK_SIZE (uint32_t) (1024*5)

uint8_t tab[IMAGE_CHUNK_SIZE];
/*uint8_t ArrayBuffer2[MAXBUFFER_SRAM1] __attribute__((section("AHBSRAM1")));
MemorySplitedArray tab(&ArrayBuffer1[0], &ArrayBuffer2[0], MAXBUFFER_LRAM1, MAXBUFFER_SRAM1);*/

DigitalOut cs(p8);
struct camHelper {

    SPI *spi;
    DigitalOut *cs;
    I2C *i2c;
    unsigned int g_frame_size_in_bytes;
    uint8_t *pbuffer;
    LocalFileSystem *local;
    unsigned int currentPosition;
    uint8_t temp;
    uint8_t temp_last;
    bool ENDOFJPG;
    uint16_t countOver;

    char getByte() {
        currentPosition++;
        return tab[currentPosition-1];
    }

    void init_spi(PinName mosi = p5, PinName miso = p6, PinName sclk = p7, PinName ssel = p8) {
        spi = new SPI (mosi, miso, sclk);
        spi->frequency(5*1000000);
        cs = new DigitalOut(ssel);
    };
    void init_i2c(PinName sda = p9, PinName slc = p10) {
        i2c = new I2C(sda, slc);
    };

    int bus_write(int address, int value) {
        // take the SS pin low to select the chip:
        *cs = 0;
        //  send in the address and value via SPI:
        spi->write(address);
        spi->write(value);
        // take the SS pin high to de-select the chip:
        *cs = 1;

        return 0;
    }

    void write_reg(uint8_t addr, uint8_t data) {
        bus_write(addr | 0x80, data);
    }

    void flush_fifo(void) {
        write_reg(ARDUCHIP_FIFO, FIFO_CLEAR_MASK);
    }

    void clear_fifo_flag(void) {
        write_reg(ARDUCHIP_FIFO, FIFO_CLEAR_MASK);
    }

    void start_capture(void) {
        write_reg(ARDUCHIP_FIFO, FIFO_START_MASK);
    }

    uint8_t bus_read(int address) {
        uint8_t value = 0;
        // take the SS pin low to select the chip:
        *cs = 0;
        //  send in the address and value via SPI:
        //  printf("addr = 0x%x\r\n",address);
        spi->write(address);
        value = spi->write(0x00);
        // take the SS pin high to de-select the chip:
        *cs = 1;
        return value;
    }

    uint8_t read_reg(uint8_t addr) {
        uint8_t data;
        data = bus_read(addr);
        return data;
    }

    uint8_t get_bit(uint8_t addr, uint8_t bit) {
        uint8_t temp;
        temp = read_reg(addr);
        temp = temp & bit;
        return temp;
    }

    uint32_t read_fifo_length(void) {
        uint32_t len1,len2,len3=0;
        uint32_t length=0;
        len1 = read_reg(FIFO_SIZE1);
        len2 = read_reg(FIFO_SIZE2);
        len3 = read_reg(FIFO_SIZE3) & 0x07;
        length = ((len3 << 16) | (len2 << 8) | len1);// & 0x07ffff;
        return length;
    }

    int32_t I2CBufferWrite(int32_t ucDevAddr, uint8_t *ucBuffer,
                           int32_t ulSize,unsigned char ucFlags) {
        int32_t err = 0;
        err = i2c->write(ucDevAddr,(char*)ucBuffer,ulSize);
        if(err == 1) {
            printf("Return error I2C write\n\r");
            return -1;
        }
        wait_ms(1);
        return 0;
    }

    int32_t I2CBufferRead(int32_t ucDevAddr, uint8_t *ucBuffer,
                          int32_t ulSize, unsigned char ucFlags) {
        int32_t err = 0;

        err = i2c->read(ucDevAddr,(char*)ucBuffer,ulSize);
        if(err == 1) {
            printf("Return error I2C read\n\r");
            return -1;
        }
        wait_ms(1);
        return 0;
    }

    uint8_t rdSensorReg8_8(uint8_t regID, uint8_t* regDat) {
        uint8_t buff[20];
        buff[0] = regID;

        I2CBufferWrite(sensor_addr,buff,1, I2C_SEND_STOP);
        I2CBufferRead(sensor_addr+1,buff,1, I2C_SEND_STOP);
        *regDat = buff[0];
        return(1);
    }

    uint8_t wrSensorReg8_8(int regID, int regDat) {
        uint8_t buff[20];
        buff[0] = regID;
        buff[1] = regDat;
        I2CBufferWrite(sensor_addr,buff,2, I2C_SEND_STOP);
        return(1);
    }

    uint8_t wrSensorReg16_8(int regID, int regDat) {
        uint8_t buff[20];
        buff[0] = (regID >> 8);
        buff[1] = (regID & 0xFF);
        buff[2] = regDat;
        I2CBufferWrite(sensor_addr,buff,3, I2C_SEND_STOP);
        return(1);
    }

    int wrSensorRegs8_8(const struct sensor_reg reglist[]) {
        uint16_t reg_addr = 0;
        uint16_t reg_val = 0;
        const struct sensor_reg *next = reglist;

        while ((reg_addr != 0xff) | (reg_val != 0xff)) {
            reg_addr = next->reg;
            reg_val = next->val;
            wrSensorReg8_8(reg_addr, reg_val);
            next++;

        }

        return 1;
    }

    void initCam() {
        
        currentPosition = 0;
        wrSensorReg8_8(0xff, 0x01);
        wrSensorReg8_8(0x12, 0x80);
        wait_ms(100);
        wrSensorRegs8_8(OV2640_JPEG_INIT);
        //wrSensorRegs8_8(OV2640_RGB565);
        wrSensorRegs8_8(OV2640_YUV422);
        wrSensorRegs8_8(OV2640_JPEG);
        //
        wrSensorReg8_8(0xff, 0x01);
        wrSensorReg8_8(0x15, 0x00);
        wrSensorRegs8_8(OV2640_640x480_JPEG);

        table_mask_write(OV_SETTING_BRIGHTNESS[5]);
        table_mask_write(OV2640_BRIGHTNESS_ENABLE);

        //table_mask_write(OV_SETTING_CONTRAST[0]);
        //table_mask_write(OV2640_CONTRAST_ENABLE);

        //table_mask_write(OV_SETTING_HUE[0]);
        //table_mask_write(OV2640_HUE_ENABLE);

        //table_mask_write(OV_SETTING_SATURATION[0]);
        //table_mask_write(OV2640_SATURATION_ENABLE);


        //table_mask_write(OV_SETTING_SHARPNESS[6]);
        //table_mask_write(OV2640_SHARPNESS_MANUAL);

        //table_mask_write(OV_SETTING_WHITEBALANCE[2]);

        //table_mask_write(OV_SETTING_SPECIALEFFECT[0]);

        //table_mask_write(OV_SETTING_AUTOEXPOSURE[0]);

    }

    void exposureTime(uint16_t us) {
        uint16_t a = ceil(us/53.39316);

        uint8_t a1 = a >> 16;
        uint8_t a2 = (a >> 8) & 0xff;
        uint8_t a3 = a & 0xff;

        wrSensorReg16_8(0x3503, 0x05);
        wrSensorReg16_8(0x3500, a1);
        wrSensorReg16_8(0x3501, a2);
        wrSensorReg16_8(0x3502, a3);
    }

    void set_fifo_burst() {
        spi->write(BURST_FIFO_READ);
    }

    uint8_t read_fifo_burst() {

        uint32_t bytesRead = 0;
        uint8_t temp = 0;
        uint8_t temp_last = 0;
        int32_t i;

        *cs = 0;
        set_fifo_burst();
        spi->write(0x00);//Ignore first byte
        i = 0;
        while( 1 ) {
            temp_last = temp;
            temp = (uint8_t )spi->write(0x00);
            tab[i] = temp;
            i++;
            bytesRead++;
            if( temp == 0xD9 && temp_last == 0xFF ) {
                break;
            }
            if(i>= IMAGE_BUF_SIZE) {
                break;
            }
        }
        g_frame_size_in_bytes = bytesRead;
        *cs = 1;

        return 1;
    }

    uint8_t read_fifo_test(size_t max) {
        uint32_t bytesRead = 0;
        int32_t i;

        *cs = 0;
        //set_fifo_burst();
        //spi->write(0x00);//Ignore first byte
        i = 0;
        while( 1 ) {
            temp_last = temp;
            //
            temp = bus_read(SINGLE_FIFO_READ);
            tab[i] = temp;
            i++;
            bytesRead++;
            if( temp == 0xD9 && temp_last == 0xFF ) {
                printf("END OF JPG\n\r");
                ENDOFJPG = true;
                break;
            }
            if(i>= IMAGE_BUF_SIZE) {
                printf("Buff MAX\n\r");
                break;
            }
            if(i >= max) {
                printf("TMP READ MAX\n\r");
                break;
            }
        }
        g_frame_size_in_bytes = bytesRead;
        *cs = 1;

        return 1;
    }

    void camId() {
        uint8_t vid, pid;

        rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
        rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
        if((vid != 0x26) || (pid != 0x42)) {
            printf("Can't find OV2640 module! vid 0x%x pid 0x%x\r\n",vid, pid);
            while(1);
        } else {
            printf("OV2640 detected\r\n");
        }
    }

    void init() {
        printf("init cam spi\n\r");
        init_spi();
        printf("init cam i2c\n\r");
        init_i2c();
        /*printf("find cam\n\r");
        camId();*/

        printf("init cam\n\r");
        initCam();
    };

    void takePicture() {
        countOver = 0;
        ENDOFJPG = false;
        temp = 0;
        temp_last = 0;
        currentPosition = 0;
        pbuffer = &(tab[0]);
        printf("flush fifo\n\r");
        flush_fifo();
        wait_ms(5);
        printf("clear fifo\n\r");
        clear_fifo_flag();
        printf("start capture\n\r");
        start_capture();
        printf("capt...\n\r");
        while (!(get_bit(ARDUCHIP_TRIG, CAP_DONE_MASK))) {
            wait_ms(100);
        }
    }

    void savePicture() {
        uint8_t* Image = reinterpret_cast<uint8_t*>(pbuffer);
        int inl = read_fifo_length();
        printf("fifo size : %d\n\r", inl);
        read_fifo_burst();
        printf("\n\r\n\r");

        FILE *fp = fopen("/local/out.jpg", "w");  // Open "out.txt" on the local file system for writing

        int x = 0;
        printf("%d\n\r", g_frame_size_in_bytes);

        while(x < g_frame_size_in_bytes) {
            fprintf(fp, "%c", Image[x++]);
            //wait_ms(1);
            //printf("0x%02x ",(char *)g_image.g_image_buffer[x++]);
        }
        fclose(fp);
    }

    camHelper() {
        local = new LocalFileSystem("local");
    };

    int table_mask_write(const uint8_t* ptab) {
        uint8_t  address;
        uint8_t  value,orgval;
        uint8_t  mask;
        const uint8_t *pdata=ptab;

        if ( NULL==pdata )
            return -1;

        while(1) {
            address =*pdata++;
            value = *pdata++;
            mask = *pdata++;
            if ( (0==address) && (0==value) &&(0==mask) ) {
                break;
            }

            if(mask != 0xFF) {

                rdSensorReg8_8(address ,&orgval);
                orgval &= (~mask);
                value  &= mask;
                value |=orgval;
            }

            //printf("adress = %8x  value = %8x \n", address,value);
            wrSensorReg8_8(address,value);
        }

        return 0;
    }
};